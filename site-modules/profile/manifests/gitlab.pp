# Super-simple profile that uses puppet/gitlab module to set up GitLab.
# Optionally uses the puppetlabs-firewall module to open a couple ports.

class profile::gitlab (
  Boolean $manage_firewall = false,
  String  $package_ensure  = 'installed',
) {

  # Include the gitlab module's main class, setting the package version
  class { 'gitlab':
    package_ensure => $package_ensure,
  }

  # Add bash command completion for git
  # The git.sh file is part of the git package.
  file { '/etc/profile.d/git.sh':
    ensure => 'link',
    target => '/etc/bash_completion.d/git',
  }

  # If desired, add a firewall opening.
  if ( $manage_firewall ) {
    include firewall
    firewall { '030 accept http and https':
      port   => [80,443],
      proto  => tcp,
      action => accept,
    }
  }

}

